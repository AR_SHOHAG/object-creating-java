class Cons
{
	double width,height,depth;

	Cons(double w,double h, double d)
	{
		width = w;
		height = h;
		depth = d;
	}

	void volume()
		{
			System.out.println("Volume = "+ width*height*depth);
		}
}

class Cons1
{
	public static void main(String args[])
	{
		Cons b= new Cons(10,20,30);
		b.volume();
	}
}
